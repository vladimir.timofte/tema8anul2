<?php
require('autovehicul.class.php');

class Masina extends Autovehicul {
    public $manufacturer;

    public function __construct($manufacturer) {
      $this->manufacturer = $manufacturer;
    }

    public function set_nume($manufacturer) {
      $this->manufacturer = $manufacturer;
    }

    public function get_nume() {
      return $this->manufacturer;
    }

    public function isOn($param){
      if($param==0){
        echo 'Masina nu e pe pozitia de miscare';
      } else if($param==1){
        echo 'Masina e pe pozitia de miscare';
      } else {
        echo 'Unknown action';
      }
    }
  }
?>
