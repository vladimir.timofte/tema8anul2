<?php
require('autovehicul.class.php');

class Bicicleta extends Autovehicul {
    public $brand;

    public function __construct($brand) {
      $this->brand = $brand;
    }

    public function set_nume($brand) {
      $this->brand = $brand;
    }

    public function get_nume() {
      return $this->brand;
    }

    public function isMoving($param){
      if($param==0){
        echo 'Bicicleta nu se misca';
      } else if($param==1){
        echo 'Bicicleta se misca';
      } else {
        echo 'Unknown action';
      }
    }
  }
?>
