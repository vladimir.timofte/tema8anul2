<?php
include('transport.class.php');

class Avion extends Transport {
    public $linieDeAvion;

    public function __construct($linieDeAvion) {
      $this->linieDeAvion = $linieDeAvion;
    }

    public function set_nume($linieDeAvion) {
      $this->linieDeAvion = $linieDeAvion;
    }

    public function get_nume() {
      return $this->linieDeAvion;
    }
  }
?>
