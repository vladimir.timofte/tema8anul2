<?php
include('transport.class.php');

class Tren extends Transport {
    public $railway;

    public function __construct($railway) {
      $this->railway = $railway;
    }

    public function set_nume($railway) {
      $this->railway = $railway;
    }

    public function get_nume() {
      return $this->railway;
    }
  }
?>
